import $ from 'jquery';


export default function(){
  $('[data-fancybox="review"]').fancybox({
    backFocus : false,
    parentEl: '.review-fancybox-container',
    infobar: false,
    buttons: [],
    btnTpl: {
      arrowLeft:
        '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' +
          '<div><svg xmlns="http://www.w3.org/2000/svg" style="transform: rotateY(180deg);" width="18" height="31" fill="none" viewBox="0 0 18 31"><path fill="#fff" d="M17.453 16.555c.586-.586.586-1.407.117-1.992L3.742.969C3.156.383 2.22.383 1.75.969l-.82.82c-.586.586-.586 1.406 0 1.992L12.883 15.5.93 27.336c-.586.586-.586 1.406 0 1.992l.82.82c.469.586 1.406.586 1.992 0l13.711-13.593z"/></svg></div>' +
        '</button>',
      arrowRight: 
        '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' +
          '<div><svg xmlns="http://www.w3.org/2000/svg" width="18" height="31" fill="none" viewBox="0 0 18 31"><path fill="#fff" d="M17.453 16.555c.586-.586.586-1.407.117-1.992L3.742.969C3.156.383 2.22.383 1.75.969l-.82.82c-.586.586-.586 1.406 0 1.992L12.883 15.5.93 27.336c-.586.586-.586 1.406 0 1.992l.82.82c.469.586 1.406.586 1.992 0l13.711-13.593z"/></svg></div>' +
        '</button>'
    },
    afterClose : function() {
    }
  });
  
  $(document).on('click', '.review--carousel .slick-cloned', function(e) {
    var $slides = $(this)
      .parent()
      .children('.slick-slide:not(.slick-cloned)');

    $slides
      .eq( ( $(this).attr("data-slick-index") || 0) % $slides.length )
      .trigger("click.fb-start", { $trigger: $(this) });

    return false;
  });

  $(".review--carousel .item_wrapper .content_wrapper").each(function(){
    var $item = $(this);
    if($(this).height() > 185) {
      $(this).addClass('read_more');
    }
  })

  $(document).on('click', '.fancybox-container .review-item .image_wrapper.video_cover', function(){
    $(this).find('img').addClass('fadeout');
    $(this).find('.video_container').addClass('visible');
    var $target = $(this).find('.video_container').find('iframe');
    $target.attr('src', $target.attr('src') + "&autoplay=1&playsinline=1&mute=1");
    if($(window).width() < 980) {
      $(this).css('display', 'block');
    }
  })

}