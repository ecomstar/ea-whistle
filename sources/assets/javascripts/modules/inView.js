import $ from 'jquery';
import inView from 'in-view';

export default function() {
  
  inView('.btn-checkout button[data-checkout-btn]')
  .on('enter', el => {
    $('.sticky-checkout').removeClass('visible');
  })
  .on('exit', el => {
    $('.sticky-checkout').addClass('visible');
  });

}
