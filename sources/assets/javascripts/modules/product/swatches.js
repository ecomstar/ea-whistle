import $ from 'jquery';
import { formatMoney } from '/tools/moneyFormats';

const $variantsSelector = $('[data-all-variants]');
const $productOption = $('[data-product-option]');
const $qtySelector = $('[data-qty]');

const updateProduct = function ($productForm) {
  const $variantSelected = $('option:selected', $productForm.find('[data-all-variants]'));
  $("[data-selected-variant-id]").val($variantSelected.val());
  updatePrice($variantSelected, $productForm);
  updateOptions($variantSelected, $productForm);
  updateButton($variantSelected, $productForm);
  updateLowStock($variantSelected, $productForm);
  updateTopLabel($variantSelected, $productForm);
  updateImage($variantSelected, $productForm);
  // updateHistoryState($variantSelected.val());
  // updateRecharge($variantSelected);
}

const updateHistoryState = function(variant) {
  if (!history.replaceState || !variant) {
    return;
  }
  var newurl =
    window.location.protocol +
    '//' +
    window.location.host +
    window.location.pathname +
    '?variant=' +
    variant;
  window.history.replaceState({ path: newurl }, '', newurl);
}

const updateImage = function($variantSelected, $productForm) {
  
  // console.log($variantSelected.val());
  const slideIndex = $('.product-slider [data-variant=' + $variantSelected.val() + ']').data('index');  
  $('.product-slider [data-variant]').each(function(index, slide){    
    if($(slide).data('variant')) {
      // console.log('xxxx', $variantSelected.val());
      if($(slide).data('variant').includes($variantSelected.val())) {        
        $('.product-slider').slick('slickGoTo', $(slide).data('index'));
      }
    }
  })
  
}

const updatePrice = function($variantSelected, $productForm) {
  const $price = $productForm.find('[data-price]');
  const $priceCompare = $productForm.find('[data-price-compare]');
  var price = $variantSelected.data('variant-price');
  const comparePrice = $variantSelected.data('variant-compare-price');

  if($productForm.attr('data-type') == 'fit' || $productForm.attr('data-type') == 'explore') {
    // price = parseFloat($productForm.find('.pricing-options .pricing-option.active').attr('data-option-price')) + parseFloat($productForm.find('.plan-options .plan-option.active').attr('data-plan-price'));
  }

  $price.text(formatMoney(price));

  comparePrice > 0 ? $priceCompare.text(formatMoney(comparePrice)).show() : $priceCompare.hide();

  // $productForm.find('[data-pdp-atc] .price').text(price);
}

const updateButton = function($variantSelected, $productForm) {
  const $addToCart = $productForm.find('[data-add-to-cart]');
  var $btn_preorder = $productForm.find('[data-pdp-atc-preorder]');
  if ($variantSelected.data('subscription-id') != '') {
    let text = window.theme.strings.recharge;
    // if ($variantSelected.data('product-handle').indexOf('annual') != -1) {
    //   let price = formatMoney($variantSelected.data('variant-price'));
    //   text += ' - '+price;
    // }
    // $addToCart.attr('disabled', false).text(text);
  } else if ($variantSelected.data('available') == true) {
    if ($variantSelected.data('inventory-policy') == 'continue' && $variantSelected.data('inventory-quantity') <= 0) {
      $addToCart.attr('disabled', false);
      // $addToCart.find('span').text(window.theme.strings.preOrder);
      $addToCart.find('span').text(window.theme.strings.addToCart);
      $btn_preorder.attr('disabled', false);
      // $btn_preorder.find('.label').text(window.theme.strings.preOrder);
      $btn_preorder.find('.label').text(window.theme.strings.addToCart);
    } else {
      $addToCart.attr('disabled', false);
      $addToCart.find('span').text(window.theme.strings.addToCart);
      // $btn.attr('disabled', false);
      // $btn.find('.label').text(window.theme.strings.addToCart);
      $("form [data-choose-plan]").attr('disabled', false);
      $("form [data-choose-plan] span").text('Choose your plan');
    }
  } else {
    $addToCart.attr('disabled', true);
    $addToCart.find('span').text(window.theme.strings.soldOut);
    // $btn.attr('disabled', true);
    // $btn.find('.label').text(window.theme.strings.soldOut);
    $("form [data-choose-plan]").attr('disabled', true);
    $("form [data-choose-plan]").find('span').text(window.theme.strings.soldOut);
  }
}

const updateTopLabel = function($variantSelected, $productForm) {
  const $topLabel = $productForm.find('.product-info__top-label').find('span');
  // if ($variantSelected.data('product-handle').indexOf('annual') != -1 && $variantSelected.data('subscription-id') != '' && $topLabel.length) {
  //   let text = window.theme.strings.recharge;
  //   let price = formatMoney($variantSelected.data('variant-price'));
  //   text += ' - ' + price;
  //   text = $(`<b>${text}</b>`);
  //   $topLabel.html(text);
  // }
}

const updateLowStock = function($variantSelected, $productForm) {
  const $lowStock = $productForm.find('[data-low-stock]');
  if ($variantSelected.data('available')) {
    $lowStock.removeClass('hide');
  } else {
    $lowStock.addClass('hide');
  }
}

const updateOptions = function($variantSelected, $productForm) {
  // Update option 1
  $('[data-product-option=1]').each(function (index, option1) {
    const $productForm = $(this).closest('[data-product-form]');
    let disabled = true;
    const optionVal = $(option1).val();
    $productForm.find('[data-all-variants]').find('option').each(function (index, selectOption) {
      if ($(selectOption).data('option1') == optionVal && $(selectOption).data('available') == true) {
        disabled = false;
      }
    });
    if (disabled) {
      // $(option1).attr('disabled', true);
      $(option1).addClass('is-disabled');
    }
  });

  // Update options 2 and 3
  const option1 = $variantSelected.data('option1');
  $productForm.find('[data-all-variants]').find('option').each(function (index, selectOption) {
    if ($(selectOption).data('option1') == option1) {
      const option2 = $(selectOption).data('option2');
      const $option2 = $(`[data-product-option=2][value='${option2}']`);
      const option3 = $(selectOption).data('option3');
      const $option3 = $(`[data-product-option=3][value='${option3}']`);
      if ($(selectOption).data('available') == false) {
        // $option2.attr('disabled', true);
        // $option3.attr('disabled', true);
        $option2.addClass('is-disabled');
        $option3.addClass('is-disabled');
      } else {
        $option2.attr('disabled', false);
        $option3.attr('disabled', false);
      }
    }
  });
}

export default function() {
  // Variants changing (automatically triggers from options changing)
  $(document).on('change','[data-all-variants]', function() {
    updateProduct($(this).closest('[data-product-form]'));
  });

  $qtySelector.on('change', function() {
    updateProduct($(this).closest('[data-product-form]'));
  });

  $(document).on('click', 'form#addToCartForm .pricing-options [data-price-option]', function(){
    var $form = $(this).closest('form#addToCartForm');
    var price = parseFloat($form.find('.pricing-options .pricing-option.active').attr('data-option-price')) + parseFloat($form.find('.plan-options .plan-option.active').attr('data-plan-price'));
    $form.find('button span[data-price]').text(formatMoney(price));
  })

  
  $(document).on('click', 'form#addToCartForm .pricing-options [data-price-option]', function(){

    var type = $(this).attr('data-price-option');
    var $form = $(this).closest('form#addToCartForm');
    var $planWrapper = $form.find('.plan-options');
    var $summaryWrapper = $form.find(".pricing-options .block_summary");
    if(! $(this).hasClass('active')) {
      $form.find('[data-price-option]').removeClass('active');
      $(this).addClass('active');
      $planWrapper.find('[data-plan-option]').addClass('disabled').removeClass('active');
      if(type == 'full-retail') {
        $planWrapper.find('[data-plan-option = "monthly"]').removeClass('disabled');
        $planWrapper.find('[data-plan-option = "monthly"]').click();
      }
      else if(type == 'plan') {
        $planWrapper.find('[data-plan-option = "plan"]').removeClass('disabled');
        $planWrapper.find('[data-plan-option = "plan"]').eq(0).click();
      }

      $summaryWrapper.find('.summary_title').text($(this).find('.heading').text());
      $summaryWrapper.find('.summary_value').text($(this).find('.price').text());

    }

    var price = parseFloat($form.find('.pricing-options .pricing-option.active').attr('data-option-price')) + parseFloat($form.find('.plan-options .plan-option.active').attr('data-plan-price'));
    $form.find('button span[data-price]').text(formatMoney(price));
  })

  
  $(document).on('click', 'form#addToCartForm .plan-options [data-plan-option]', function(){
    var $form = $(this).closest('form#addToCartForm');
    var $summaryWrapper = $form.find('.plan-options .block_summary');
    if(! $(this).hasClass('disabled') && ! $(this).hasClass('active')) {
      $form.find('[data-plan-option]').removeClass('active');
      $(this).addClass('active');

      $summaryWrapper.find('.summary_title').text($(this).find('.heading').text());
      $summaryWrapper.find('.summary_value').text($(this).find('.price').text());
    }

    var price = parseFloat($form.find('.pricing-options .pricing-option.active').attr('data-option-price')) + parseFloat($form.find('.plan-options .plan-option.active').attr('data-plan-price'));
    $form.find('button span[data-price]').text(formatMoney(price));
  })

  

  // Options changing
  $(document).on('click', '[data-product-option]', function () {
    const $productForm = $(this).closest('[data-product-form]');
    const option1 = $('[data-product-option=1]:checked').length ? $('[data-product-option=1]:checked').val() : '_BLANK_';
    const option2 = $('[data-product-option=2]:checked').length ? $('[data-product-option=2]:checked').val() : '_BLANK_';
    const option3 = $('[data-product-option=3]:checked').length ? $('[data-product-option=3]:checked').val() : '_BLANK_';
    const $vSelector = $productForm.find('[data-all-variants]');
    const value = $vSelector.find(`[data-option1='${option1}'][data-option2='${option2}'][data-option3='${option3}']`).val();
    $vSelector.val(value).trigger('change');
    // console.log('variantselector value', $vSelector.val());

    if($(this).attr('data-option-type') == 'size') {
      // $(this).closest('.product-options').find('.product-options__title').removeClass('warning')
      // $(this).closest('.product-options').find('.product-options__title label').text('Select a size: ');
      // $(this).closest('.product-options').find('[data-selected-size]').text(($(this).val()).replace('||', '-'));
    }

    var $options = $(this).closest('.product-options');
    $options.find('[data-product-option]').removeClass('selected');
    $(this).addClass('selected');

    var vName = $(this).attr('data-option-type');

    if(vName == 'size') {
      $(this).closest('.product-options').find('.product-options__title').removeClass('warning');
      $(this).closest('.product-options').find('.product-options__title label').text('Select a size: ');
      $(this).closest('.product-options').find('[data-selected-size]').text(($(this).val()).replace('||', '-'));
    }
    else if (vName == 'color') {
      $(this).closest('.product-options').find('.product-options__title').removeClass('warning');
      $(this).closest('.product-options').find('.product-options__title label').text('Select a color: ');
      $(this).closest('.product-options').find('[data-selected-color]').text($(this).val());
    }


  });

  if (window.productTemplateLoaded) {
    updateProduct($('.product-main').find('[data-product-form]'));
  }
}