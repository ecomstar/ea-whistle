import $ from 'jquery';
import 'slick-carousel';

export default function() {
  const $productSlider = $('[data-ps]');
  const $productThumbSlider = $('[data-thumb-ps]');

  if ($productSlider.length) {
    $productSlider.on('init', function() {
    })
    const slider = $productSlider.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      infinite: false,
      asNavFor: '[data-thumb-ps]'
    });


    // const thumbSlider = $productThumbSlider.slick({
    //   slidesToShow: 5,
    //   slidesToScroll: 1,
    //   arrows: false,
    //   dots: false,
    //   infinite: false,
    //   asNavFor: '[data-ps]',
    //   focusOnSelect: true
    // });

    setTimeout(function(){
      $productSlider.addClass('is-visible');
    }, 200);

    const thumbSlider = $productThumbSlider.slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      infinite: false,
      asNavFor: '[data-ps]',
      focusOnSelect: true
    });



    // var $slider = $("[data-slider-dots-overlay]");
    // $slider.on('init', function() {
    //   $slider.addClass('is-visible');
    // })

    // var length = $slider.attr('data-length');
    // const slider = $slider.slick({
    //   arrows: false,
    //   dots: true
    // })
    // .on('beforeChange', function(event, slick, currentSlide, nextSlide){
    //   // console.log('before', nextSlide);
    //   $slider.find('[data-slider-count]').html((nextSlide + 1) + ' / ' + length);
    // })
    // .on('afterChange', function(event, slick, currentSlide, nextSlide){
    //   // console.log('after', nextSlide);
    // });
    // $slider.find('ul.slick-dots').append('<span data-slider-count>' + length + ' / ' + '1' + '</span>').append('<span data-slick-next-trigger><svg xmlns="http://www.w3.org/2000/svg" width="8.346" height="14.721" viewBox="0 0 8.346 14.721"><path fill="#000" d="M8.19 13.6L1.917 7.357l6.212-6.239a.677.677 0 0 0-.955-.955L0 7.357l7.228 7.2a.677.677 0 0 0 .955-.955z" transform="rotate(180 4.173 7.36)"/></svg></span>');

    // $(document).on('click', '[data-slick-next-trigger]', function(){
    //   var $slider = $(this).closest('[data-slider-dots-overlay]');
    //   $slider.slick('next');
    // })

  }
}