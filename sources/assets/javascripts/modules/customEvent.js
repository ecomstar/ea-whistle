import $ from 'jquery';
import { updateCart } from './minicart';
import { formatMoney } from '/tools/moneyFormats';


const init = function(){

  giftUpsell();

  giftSet();
  
  bannerTabArrow();

  sweepstakesSubmit();

  cartPopup();

  cartUpsellATC();

  redirection();

  fileDownload();

  upsellATC();

  sizeGuide();

  atcRegular();

  minicartUpsellAtc();

  minicartPlanPopup();

  smsPopup();

  backInStock();

  singleItemATC();

  productFeatureIcon();

  productSwitchProgress();

  productAnchorLink();

  productDescTab();

  mobSelectPlan();

  collectionProductGrid();

  iconSlider();

  iconList();

  scrollLine();

  tabletLogoSlider();

  menuActive();

  linkTrigger();

  popup();

  blog();

  cart();

  compareModel();

  productStickyBar();

  atcChoosePlan();

  collectionTab();

  actionListener();
  
  variantChange();

  reviewCarousel();

  bannerTab();

  variableWidthSlider();

  productFeatureTab();

  productPlanPopup();

  // dotsOverlaySlider();

  productTech();

  initRelatedItem();

  ajaxATC();

}

function giftUpsell() {

  var bundle_compare_price, bundle_regular_price, bundle_save;

  var _productId = null, _planId = null;

  var _planType;

  $(document).on('click', '#shopify-section-holiday-gift-set .product-grid-item [data-popup-open]', function() {
    var $item = $(this).closest('.product-grid-item');
    bundle_compare_price = parseFloat($item.attr('data-item-total'));
    bundle_save = parseFloat($item.attr('data-item-save'));
    bundle_regular_price = bundle_compare_price - bundle_save;

    var $form = $item.find('.product_ajax_form.ajax-atc');
    var option1 = $form.find('input[data-variant-option="1"]:checked').val();
    var option2 = $form.find('input[data-variant-option="2"]:checked').val();

    if(option1 == null || option1 == undefined) {
      option1 = '_BLANK';
    }
    if(option2 == null || option2 == undefined) {
      option2 = '_BLANK';
    }
    
    var id = $form.find('[data-all-variants]').find('option[data-option1="'+ option1 +'"][data-option2="'+ option2 +'"]').val();

    $item.attr('data-product-id', id);

    _productId = $item.attr('data-product-id');
    _planId = $item.attr('data-plan-id');

    _planType = $item.attr('data-plan-type');

    calcPrice();
  })

  $(document).on('click', '#shopify-section-gift-upsell-popup .upsell-item .btn_wrapper button', function() {
    var $item = $(this).closest('.upsell-item');
    var $wrapper = $item.closest('.upsell-wrapper');
    var $buttons = $item.closest('#shopify-section-gift-upsell-popup').find('.checkout_buttons');
    $item.removeClass('disable').toggleClass('active');
    if($item.hasClass('active')) {
      $item.nextAll().removeClass('active').addClass('disable');
      $item.prevAll().removeClass('active').addClass('disable');
      $buttons.find('.upsell-checkout').removeAttr('disabled');
      $buttons.find('.regular-checkout').attr('data--hidden', 'true');
    }
    else {
      $item.nextAll().removeClass('active').removeClass('disable');
      $item.prevAll().removeClass('active').removeClass('disable');
      $buttons.find('.upsell-checkout').attr('disabled', 'true');
      $buttons.find('.regular-checkout').removeAttr('data--hidden');
    }
    setTimeout(function() {
      calcPrice();
    })
  })

  $(document).on('click', '.checkout_buttons .upsell-checkout', function() {

    var upsell_id = $(this).closest('#shopify-section-gift-upsell-popup').find('.upsell-item.active .product_atc_form').attr('data-id');
    holidayBundleATC(upsell_id);
  })

  $(document).on('click', '.checkout_buttons .regular-checkout', function() {
    holidayBundleATC();
  })
  

  function calcPrice() {
    var $popup = $("#shopify-section-gift-upsell-popup");
    var $activeItem = $popup.find('.upsell-item.active');

    var item_compare_price = 0, item_regular_price = 0;
    if($activeItem.length > 0) {
      item_compare_price = parseFloat($activeItem.find('.product_price').attr('data-compare-price'));
      item_regular_price = parseFloat($activeItem.find('.product_price').attr('data-regular-price'));
    }

    var total_comparePrice = bundle_compare_price + item_compare_price;
    var total_regularPrice = bundle_regular_price + item_regular_price;
    var total_save = total_comparePrice - total_regularPrice;

    var $totalComparePrice = $popup.find('.total_price .was-price');
    var $totalRegularPrice = $popup.find('.total_price .regular-price');
    var $totalSave = $popup.find('.total_price .save span');

    $totalComparePrice.text(formatMoney(total_comparePrice));
    $totalRegularPrice.text(formatMoney(total_regularPrice));
    $totalSave.text(formatMoney(total_save));
  }

  function holidayBundleATC(upsell_id) {
    var planName = '1-year-plan';
    var planId = _planId;

    var data = {
      "id" : planId,
      "quantity" : 1,
    };

    data['properties'] = {
      '_id': _productId,
      'type': 'Subscription',
      'device': _planType,
      '_plan': planName
    }
    
    var isATC = true;
    $.ajax({
      type: 'GET',
      url: '/cart.js',
      dataType: 'json',
      success: function(cart){
        var cartItems = cart['items'];
        
        for(var i = 0; i < cartItems.length; i ++) {
          var item = cartItems[i];
          var className = '';
          if(_planType == 'explore' || _planType == 'fit' || _planType == 'switch') {
            var itemType = item.properties.device;
            if(itemType == 'explore' || itemType == 'fit' || itemType == 'switch') {
              if(_planType != itemType) {
                var className = _planType + '-' + itemType;
                isATC = false;
                var $popup = $('[data-popup=cart-err-popup');
                $popup.find('.text_wrapper').attr('data--hidden', 'true');
                $popup.find('.text_wrapper.' + className).removeAttr('data--hidden');
                $popup.fadeIn(300);

                $('.loader_wrapper').css({"display":"none", "z-index": "-1"});
                // $handle.removeClass('loading');
                break;
              }
            }
          }
        }

        if(isATC) {

          var atcData = [];

          if(upsell_id) {
            atcData.push({
              "id": upsell_id,
              "quantity": 1
            });
          }

          atcData.push(data);

          var _data = {
            "id": _productId,
            "quantity": 1,
            "properties": {
              "device": _planType,
              "_id": _productId,
              "_plan": planName
            }
          }

          atcData.push(_data);

          $.ajax({
            type: 'POST', 
            url: '/cart/add.js',
            dataType: 'json', 
            data: {
              'items': atcData
            },
            success: function(){
              document.location.href = '/cart';
            }
          });

        }

      }
    })
  }
}

const giftSet = function() {
  $(document).on('click', '.shopify-section.gift-set-section .product-grid-item', function() {
    var $wrapper = $(this).closest('.product-grid-wrapper');
    $wrapper.find('.product-grid-item').removeClass('active');
    $(this).addClass('active');
    var index = $(this).attr('data-index');
    var $group = $(".section-group .group").removeClass('active');

    $('.section-group .group[data-group="' + index + '"]').addClass('active');
  })
}


const bannerTabArrow = function() {
  if($(window).width() < 750) {
    $(".shopify-section.banner-tab").each(function() {
      var $tab = $(this);
      var height = $('.banner_img_wrapper.hide--desktop').eq(0).height();
      setTimeout(function() {
        $tab.find('button.slick-arrow').addClass(height / 2 + 'px').css('top', height / 2 + 'px');
      }, 1000);
    })
  }
}

const sweepstakesSubmit = function() {
  $(document).on('click', '[data-sweepstakes-submit]', function(e) {
    if(! $(this).closest('form').find('input#terms_agree').prop('checked')) {
      e.preventDefault();
      $(this).closest('.main_content').find('.warning').css('display', 'block');
      return false;
    }
  })
}


const updateCartNum = function(num) {
  var $cartCount = $("[data-cart-count]");
  $cartCount.text(parseInt(($cartCount.eq(0)).text()) + num);
}

const updateCartPopup = function(img, title) {
  var $cartPopup = $(".nav-cart-wrapper [data-cart-popup]");
  if($(window).width() < 768) {
    $cartPopup = $(".mob--toolbar [data-cart-popup]");
  }
  $cartPopup.find('.item_image').html('<img src="' + img + '">');
  $cartPopup.find('.item_title').text(title);
}

const showCartPopup = function() {
  var $cartPopup = $("[data-cart-popup]");
  if($(window).width() < 768) {
    $cartPopup = $(".mob--toolbar [data-cart-popup]");
  }
  $cartPopup.show();
  setTimeout(function() {
    $cartPopup.addClass('visible');
  }, 10);
  setTimeout(function() {
    $cartPopup.fadeOut(300);
    setTimeout(function() {
      $cartPopup.removeClass('visible');
    }, 310);
  }, 3000);
}


function cartPopup() {
  $(document).on('click', '[data-cart-popup]', function() {
    window.location.href = '/cart';
  })
}

function cartUpsellATC() {
  $(".cart-wrapper .upsell-product-item .variant-colors input").on('click', function() {
    var id = $(this).attr('data-variant-id');
    $(this).closest('.upsell-product-item').find('[data-cart-upsell-add]').attr('data-variant-id', id);
    var $wrapper = $(this).closest('.upsell-product-item').find('.btn_wrapper');
    
    var $img = $(this).closest('.upsell-product-item').find('.image_wrapper img');
    $img.attr('data-srcset', $(this).attr('data-image'));
    $img.attr('srcset', $(this).attr('data-image'));

    if($(this).attr('data-added') == 'true') {
      $wrapper.addClass('added');
    }
    else {
      $wrapper.removeClass('added');
    }
  })

  $('.cart-wrapper #cart-upsell-variants').on('change', function() {
    $(this).closest('.upsell-product-item').find('[data-cart-upsell-add]').attr('data-variant-id', $(this).val());

    var $wrapper = $(this).closest('.upsell-product-item').find('.btn_wrapper');

    if($(this).find('option:selected').attr('data-added') == 'true') {
      $wrapper.addClass('added');
    }
    else {
      $wrapper.removeClass('added');
    }
  })

  $(".cart-wrapper [data-cart-upsell-add].btn-add").on('click', function() {
    var id = $(this).attr('data-variant-id');
    $.ajax({
      type: 'POST', 
      url: '/cart/add.js',
      dataType: 'json', 
      data: {
        "id": id,
        "quantity": 1,
      },
      success: function(cart){
        window.location.href = '/cart';
      }
    });
  })
}

function redirection() {
}

function fileDownload() {

  if($("[data-auto-download]").length > 0) {
    var link = document.createElement('a');
    document.body.appendChild(link);
    link.href = $("[data-auto-download]").attr('href');
    link.setAttribute('download', 'download');
    link.click();
    setTimeout(function() {
      // document.location.href = '/';
    }, 3000);
  }
}

function upsellATC() {


  var sel_variant = null;

  $(document).on('click', '[data-product-upsell-variants] input', function() {

    var $option = $(this).closest('.product_option');
    var img = $option.attr('data-variant-img');
    var id = $option.attr('data-variant-id');

    sel_variant = id;

    var $item = $(this).closest('.product-upsell-item');
    $item.find('.product-upsell-atc-form').attr('data-id', id).attr('data-variant-img', img);

    $item.find('.product-upsell-image img').attr('data-srcset', img).attr('srcset', img);

    if(! $(this).hasClass('is_checked')) {
      var handle = $(this).closest('.product-upsell-item').attr('data-handle');
      imageUpdates(handle, $(this).closest('.product-main').find('.product-images'), true, true);
    }

    var $form = $(this).closest('[data-product-variant-option]');
    if($form.attr('data-added') == 'true') {
      $item.find('.product-upsell-atc-form').attr('data-added', 'true');
    }
    else {
      $item.find('.product-upsell-atc-form').removeAttr('data-added');
    }

    $(this).closest('.product_variants-wrapper').find('input').removeClass('is_checked');
    $(this).addClass('is_checked');

  })

  $(document).on('click', '.slider_back', function(){
    
    imageUpdates(p_handle, $(this).closest('.product-main').find('.product-images'), false, false);
    // is_main_img = true;
  })

  function imageUpdates(productHandle, $imgWrapper, is_back, slickGoTo) {
    
    $imgWrapper.addClass('fade');
    $imgWrapper.css('height', $imgWrapper.height());

    $.ajax({
      url: "/products/" + productHandle,
      type: 'GET',
      data: { 
        view: 'quickview'
      },
      success: function(res) {
        let $resultDom = $(res);
        // console.log($resultDom);
        // console.log($resultDom.find('.product-images'));
        if (typeof $resultDom.find('.product-images').html() !== "undefined") {

          $imgWrapper.html($resultDom.find('.product-images').html());

          if(is_back) {
            $imgWrapper.append('<div class="slider_back"><svg xmlns="http://www.w3.org/2000/svg" width="5.58" height="12" viewBox="0 0 5.58 12"><path id="np_down-arrow_3012911_000000" fill="#231f20" d="M6.02 5.58a.741.741 0 0 1-.28-.08L.142.7A.389.389 0 0 1 .1.142.389.389 0 0 1 .662.1L6.02 4.66 11.338.1a.389.389 0 0 1 .56.04.386.386 0 0 1-.04.56L6.26 5.5a.429.429 0 0 1-.24.08z" transform="rotate(-90 6 6)"></path></svg>Back</div>');
          }

          const $productSlider = $imgWrapper.find('[data-ps]');
          const $productThumbSlider = $imgWrapper.find('[data-thumb-ps]');

          const slider = $productSlider.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            infinite: false,
            asNavFor: '[data-thumb-ps]'
          });
      
          const thumbSlider = $productThumbSlider.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            infinite: false,
            asNavFor: '[data-ps]',
            focusOnSelect: true
          });

          setTimeout(function(){
            $productSlider.addClass('is-visible');
            $imgWrapper.removeClass('fade');
            $imgWrapper.css('height', 'auto');
            if(slickGoTo) {
              var $variantSelected = $imgWrapper.closest('.product-main__container').find('form#addToCartForm select[data-all-variants] option:selected');
              // console.log('yyy', $variantSelected.val());
              $imgWrapper.find('[data-variant]').each(function(index, slide){    
                if($(slide).data('variant')) {
                  // if($(slide).data('variant').includes($variantSelected.val())) {       
                  if($(slide).data('variant').includes(sel_variant)) {       
                    $productSlider.slick('slickGoTo', $(slide).data('index'));
                  }
                }
              })
            }
          }, 200);
        }
      }
    });
  }


  $(document).on('click', '.product-upsell-atc-form .btn-add', function(e) {
    e.preventDefault();
    var $item = $(this).closest('.product-upsell-item');
    var $form = $(this).closest('.product-upsell-atc-form');
    var vId = $form.attr('data-id');
    var vImg = $form.attr('data-variant-img');
    var title = $(this).closest('.product-upsell-item').find('.product-upsell-item-title').text();

    // updateCartPopup(vImg, title);

    $.ajax({
      type: 'POST', 
      url: '/cart/add.js',
      dataType: 'json', 
      data: {
        "id": vId,
        "quantity": 1,
      },
      success: function(cart){
        var count = parseInt($(".m-cart [data-cart-count]").text()) + 1;
        $("[data-cart-count]").text(count);
        $form.attr('data-added', 'true');

        $item.find('.product_variants_color input:checked').closest('[data-product-variant-option]').attr('data-added', 'true');

        // showCartPopup();

      }
    });
  })

  $(document).on('click', '.upsell-product-item .btn_wrapper.added .btn-added', function(e) {
    e.preventDefault();
    var vId = $(this).attr('data-variant-id');
    var updateQuery = 'updates[' + vId + ']=0';
    jQuery.post('/cart/update.js', updateQuery, function(){
      // var count = parseInt($(".m-cart [data-cart-count]").text()) - 1;
      // $("[data-cart-count]").text(count);
      // $item.removeClass('added');
      window.location.href = '/cart';
    });
  })

  $(document).on('click', '.product-upsell-item .btn-added', function(e) {
    e.preventDefault();
    var $item = $(this).closest('.product-upsell-item');
    var $form = $(this).closest('.product-upsell-atc-form');
    var vId = $form.attr('data-id');
    var updateQuery = 'updates[' + vId + ']=0';
    jQuery.post('/cart/update.js', updateQuery, function(){
      var count = parseInt($(".m-cart [data-cart-count]").text()) - 1;
      $("[data-cart-count]").text(count);
      $form.removeAttr('data-added');
      $item.find('.product_variants_color input:checked').closest('[data-product-variant-option]').removeAttr('data-added');
    });
  })
}

function sizeGuide() {
  $(document).on('click', '[data-size-guide-toggle]', function() {
    $("html, body").toggleClass('no-scroll');
    $("[data-size-guide]").toggleClass('is-open');
  })

  $(document).on('click', '.body-bg', function() {
    $("html, body").toggleClass('no-scroll');
    $("[data-size-guide]").toggleClass('is-open');
  })

  $(document).on('click', '.size-guide-wrapper [data-minicart-toggle]', function() {
    $(this).closest('.size-guide').removeClass('is-open');
  })
}

function atcRegular() {
  $(document).on('click', '[data-regular-add-to-cart]', function(e) {

    var $form = $(this).closest('form');
    
    var _flag = true;
    var $size = $form.find('.product-options.m-size');
    var $s_label = $size.find('.product-options__title');
    if($size.length > 0) {
      var _target = $size.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $s_label.addClass('warning');
        $s_label.find('label').text('Please select a size');
        _flag = false;
      }
    }

    var $color = $form.find('.product-options.m-color');
    var $c_label = $color.find('.product-options__title');

    if($color.length > 0) {
      var _target = $color.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $c_label.addClass('warning');
        $c_label.find('label').text('Please select a color');
        _flag = false;
      }
    }

    if(_flag) {
     $(this).attr('data-loader-btn', true).addClass('loading');
    }


  })
}

function minicartUpsellAtc() {
  $(document).on('click', '.minicart-main[data-upsell="true"] .upsell-item', function() {
    let $item = $(this);
    let id = $item.attr('data-id');
    $.post('/cart/update.js',
      'updates[' + id + ']=0',
      function() {
        // updateCart();
        document.location.href = '/cart';
      }
    );
  })

  if($(window).width() < 990) {
    $(document).on('click', '.minicart-main[data-upsell="false"] .upsell-item', function() {
      let $item = $(this);
      let id = $item.attr('data-id');
      $.ajax({
        type: 'POST', 
        url: '/cart/add.js',
        dataType: 'json', 
        data: {
          "id": id,
          "quantity": 1,
          "properties": {
            _type: 'upsell',
            accessory: 'accessory'
          }
        },
        success: function(){
          // updateCart();
        }
      });
    })
  }
  else {
    $(document).on('click', '.btn-upsell-atc', function() {
      let $item = $(this).closest('.upsell-item');
      let id = $item.attr('data-id');
      $.ajax({
        type: 'POST', 
        url: '/cart/add.js',
        dataType: 'json', 
        data: {
          "id": id,
          "quantity": 1,
          "properties": {
            _type: 'upsell',
            accessory: 'accessory'
          }
        },
        success: function(){
          // updateCart();
          document.location.href = '/cart';
        }
      });
    })
  }
}

function minicartPlanPopup() {
  $(document).on('click', '[data-minicart-popup-trigger]', function() {
    $(this).closest('[data-minicart-popup]').toggleClass('active');
  })
  var flag = false;
  $(document).on('mouseover', '[data-minicart-popup]', function() {
    flag = true;
  })
  $(document).on('mouseleave', '[data-minicart-popup]', function() {
    flag = false;
  })
  $(document).on('click', '*', function() {
    if(!flag) {
      $('[data-minicart-popup]').removeClass('active');
    }
  })
}

function smsPopup() {
  const d = new Date();

  var cookieTime = parseInt(getCookieData('time'))
  var currentTime = d.getTime();

  if(! $("body").hasClass('template-index')) {  
    return false;
  }

  if(! cookieTime > 0 || ( currentTime - cookieTime ) / 1000 / 3600 / 24 >= 7) {
    setTimeout(function(){
      $('[data-popup="sms-popup"]').fadeIn(300);
    }, 15000);
  }


  var cookieData = {};
  $(document).on('click', '[data-sms-close]', function(){
    document.cookie = 'time = ' + d.getTime();
  })


  $(document).on('click', '[data-submit]', function() {
    console.log("hereer");
    var $wrapper = $(this).closest('.popup_form ').find('.form_wrapper');
    $wrapper.find('.sms_form .smsb-subscribe-button.cw-btn').click();

    var $iterable_form = $wrapper.find('.iterable_form');
    var $email = $iterable_form.find('input[name="email"]');
    if($email.val() == '') {
      $iterable_form.find('.error_msg').removeAttr('data--hidden');
    }
    else {
      $iterable_form.find('.error_msg').attr('data--hidden', true);
      $iterable_form.find('input[type="submit"]').click();
    }

  })
  
}

function getCookieData(name){
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : {};
} 

function backInStock() {
  $(document).on('click', '[data-bis-trigger]', function(e){
    e.preventDefault();
    $(".bis-button.BIS_trigger").click();
    // BISPopover.show() 
    // BISPopover.show({variantId: 39481877069872})
  })
}

function singleItemATC() {

  $(document).on('click', '[data-single-atc]', function(){
    var id = window.localStorage.getItem('id');
    
    var img = $(".product-main form#addToCartForm [data-all-variants] option:selected").attr('data-variant-image');

    $.ajax({
      type: 'POST', 
      url: '/cart/add.js',
      dataType: 'json', 
      data: {
        "id": id,
        "quantity": 1
      },
      success: function(){
        document.location.href = '/cart';
      }
    });
  })
  
}

function productFeatureIcon() {
  if($(window).width() < 1024) {
    $('.product-main__left .hide--mobile .shipping-features').remove();
  }
}

function productSwitchProgress() {
  var now = new Date();
  var start = new Date(now.getFullYear(), 0, 0);
  var diff = now - start;
  var oneDay = 1000 * 60 * 60 * 24;
  var day = Math.floor(diff / oneDay);

  var idx = day % 30;

  var $target = $('[data--progress]');
  var min = parseInt($target.attr('data-min'));
  var max = parseInt($target.attr('data-limit'));


  var p_val = Math.round(min + (100 - min) / 30 * idx);
  var r_val = Math.round(max / 100 * p_val);

  // $target.find('[data-val]').text(r_val);
  $target.find('[data-percent-val]').text(min + '%');
  $target.find('.progress-bar span').css('width', min + '%');

}

function productAnchorLink() {
  var href = window.location.href;
  var $target = $("#shopify-section-custom-product-reviews");
  if($target.length > 0 && href.indexOf('/#review') >= 0) {
    setTimeout(function(){
      scrollPage($target.offset().top, 1000);
    }, 500);
  }

  $(document).on('click', '.product-review-rating .star-clickable a', function(){
    var href = window.location.href;
    if(href.indexOf('/#review') >= 0) {
      if($target.length > 0) {
        scrollPage($target.offset().top, 1000);
      }
    }
    else {
      if($target.length > 0) {
        scrollPage($target.offset().top, 1000);
        setTimeout(function(){
          window.history.pushState("", "", window.location.href + '/#review');
        }, 1500);
      }
    }
  })
}

function productDescTab() {
  $(document).on('click', '.product-feature-tabs .feature-tab-header[data-feature="true"]', function(){
    var idx = $(".product-images .slider.product-slider").find('img[data-feature]').attr('data-index');
    // console.log(idx);
    $(".product-images .slider.product-slider").slick('slickGoTo', idx);
  })
}


var planFlag = true;

function mobSelectPlan() {
  if($(window).width() < 768) {
    if(planFlag) {
      $(".plan_items .plan-item .item_wrapper").on('click', function(){
        planFlag = false;
        $(this).parent().find("[data-ajax-form] button").click();
      })
      $(".choose-plan .link_btn").on('click', function(){
        planFlag = false;
        $(this).find('a').click();
      })
    }
  }
}


function collectionProductGrid() {
  var $wrapper = $("[data-product-grid-wrapper]");
  if($wrapper.length > 0) {
    $wrapper.find('section').html($("[data-product-grid] section").html());
    $("[data-product-grid]").html('');
    $("[data-product-grid]").remove();
  }
}


function iconSlider() {
  
}

function iconList() {
  if($(window).width() < 768) {
    $(".icon-list.mob-auto-slider .list-slider.desktop").remove();
  }
  else {
    if($(window).width() < 1200) {
      $(".icon-list.mob-auto-slider .icon-list-wrapper").slick({
        slidesToShow: 3,
        dots: true,
        arrows: false
      });
    }
  }
}

function scrollLine() {
  var $parent = $(".scroll-line");
  if($parent.length > 0) {
    $parent.find('.list__item').each(function(){
      $(this).find('p').each(function(idx) {
        if(idx > 0) {
          $(this).attr('data-animate-block', 'true');
        }
      })
    })
  }
}

function tabletLogoSlider() {
  var width = $(window).width();
  if(width < 1024 && width >= 768) {
    $(".shopify-section.logo-list [data-hor-slider").slick({
      slidesToShow: 2,
      dots: true,
      arrows: false
    })
  }
}

function menuActive() {
  $("[data-active-history='true']").on('click', function(){
    if($(this).attr('data-active-history') == 'true') {
      localStorage.setItem('link', $(this).attr('data-handle'));
    }
  })
  $("[data-active-history='true'] + .mega-menu__section .menu_block_wrapper a").on('click', function(){
    localStorage.setItem('link', $(this).closest('.mega-menu__section').prev('a').attr('data-handle'));
  })
  $("[data-active-history='true'] + [data-dropdown-parent] .nav-dropdown__feature-item a").on('click', function(){
    localStorage.setItem('link', $(this).closest('[data-dropdown-parent]').prev('a').attr('data-handle'));
  })

  // $("#shopify-section-ea-footer .menu-item a").on('mouseover', function(){
  //   var $target = $("header .nav__items a[href='"+ $(this).attr('href') +"']");
  //   alert($target.length);
  //   if($target.length > 0) {
  //     alert();
  //     var link_handle = '';
  //     var handle = $target[0].attr('data-active-history');
  //     if(handle == 'true') {
  //       link_handle = $target[0].attr('data-handle');
  //     }
  //     else if (handle == 'false') {

  //     }
  //     else {
  //       if($target[0].closest('.mega-menu__section').length > 0) {
  //         link_handle = $target[0].closest('.mega-menu__section').prev('a').attr('data-handle');
  //       }
  //     }
  //     localStorage.setItem('link', link_handle);
  //   }
  // })


  var link = localStorage.getItem('link');
  if(link != 'index' && link != null) {
    $("a[data-handle='" + link + "']").addClass('menu-active');
  }
}

function linkTrigger() {
  var handle = true;
  $("[data-link-disable]").on('mouseover', function(){
    handle = false;
  })
  $("[data-link-disable]").on('mouseleave', function(){
    handle = true;
  })
  $("[data-link-trigger]").on('click', function(){
    if(handle == true) {
      window.location.href = $(this).attr('data-target');
    }
  })
}

function popup() {
  $(".mega-menu__section a[href='#']").on('click', function(){
    var $popup = $('[data-popup="compare-model"]');
    $popup.fadeIn(300)
  })
}

function blog() {
  $("#shopify-section-custom-blog-main [data-tab]").on('click', function(){
    var handle = $(this).attr('data-tab');
    var $header_wrapper = $(this).closest('.tab-header-wrapper');
    var $header = $header_wrapper.find('[data-tab]');
    $header.removeClass('active');
    $(this).addClass('active');
    var $content_wrapper  = $(this).closest('#shopify-section-custom-blog-main').find('.tab-content-wrapper');
    var $content = $content_wrapper.find('article');
    $content.attr('data--hidden', 'true');
    if(handle == 'all') {
        $content.removeAttr('data--hidden');
    }
    else {
        $content_wrapper.find('article[data-type="'+ handle +'"]').removeAttr('data--hidden');
    }
  })

  if($(window).width() < 768) {
    $("[data-blog-header-slider").slick({
      arrows: true,
      dots: false,
      variableWidth: true
    })
  }
}

function cart() {
  if($(window).width() < 768) {
    var $obj = $("body.template-cart .supports-cookies.cart-empty");
    if($obj.length == 0 && $("body.template-cart").length > 0) {
      $("body").addClass('spacing');
    }
  }
}

function compareModel() {

  $(".compare-model-popup [data-scroll-wrapper]").scroll(function(){
    var posX = $(this).scrollLeft();
    var x = parseInt($(this).attr('data-len')) / 2;
    // console.log( posX * x);
    $(this).closest('.compare-model-wrapper').find('.rows_header, .rows_info, .compare_item .compare_list').animate({
      scrollLeft: posX * x
    }, 1);
  })

  $(document).on('click', '.compare_item.mob_hidden.heading_item .compare_title, .compare_item.mob_hidden.heading_item .compare_title .icon', function(){
    var handle = $(this).closest('.compare_item').attr('data-handle');
    var $target = $(this).closest('.model-content').find('.compare_item.mob_hidden[data-handle="' + handle + '"]');
    $(this).closest('.model-content').find('.compare_item.mob_hidden').removeClass('visible');
    $target.each(function(idx){
      $(this).toggleClass('visible');
      if(idx + 1 == $target.length) {
        $(this).addClass('item-bottom');
      }
      $(".mob-model-header .mob-header-item.active").click();
    })
  })

  $(document).on('click', '.mob-model-header .mob-header-item', function(){
    $(this).closest('.mob-model-header').find('.mob-header-item').removeClass('active');
    $(this).addClass('active');
    var width = $(this).closest('.compare-model-wrapper').find('.compare_item:first-child .compare-list-item').width() + 20;
    var x = parseInt($(this).attr('data-idx'));
    $(this).closest('.compare-model-wrapper').find('.rows_header, .rows_info, .compare_item .compare_list').animate({
      scrollLeft: width * (x % 3)
    }, 1);
    $(this).closest('.compare-model-wrapper').find('.footer-wrapper .btn_wrapper').removeClass('active');
    $(this).closest('.compare-model-wrapper').find('.footer-wrapper .btn_wrapper[data-idx="' + x + '"]').addClass('active');

    $(this).closest('.compare-model-wrapper').find('.mob-model-headings .heading-item').removeClass('active');
    $(this).closest('.compare-model-wrapper').find('.mob-model-headings .heading-item[data-idx="' + x + '"]').addClass('active');
  })

  if($(window).width() < 990) {
    $('.mob-model-header .mob-header-item[data-idx="1"]').click();
  }

  $(".compare-model-popup [data-read-more]").on('click', function(){
    var is_open = $(this).attr('data-open');
    if(is_open == 'true') {
      $(this).attr('data-open', 'false');
      $(this).find('span').text('See more details');
    }
    else {
      $(this).attr('data-open', 'true');
      $(this).find('span').text('See less')
    }
    var target = $(this).closest('.compare-model-popup').find('.model-content-main .compare_item.mob_hidden');
    target.toggleClass('visible');
  })


  if($(window).width() < 768) {
    setHeight();


    function setHeight() {
      var $wrapper = $(".model-row-wrapper");
      $wrapper.find('.model-row-header > div').each(function(idx) {
        var h = $(this).outerHeight();
        var $item = $(this);
        $wrapper.find('.model-row-group').each(function(i) {
          $(this).find('.model-subrow').each(function() {
            var $target = $(this).find('> div').eq(idx);
            if($target.outerHeight() > h) {
              $item.css('height', $target.outerHeight());
              h = $target.outerHeight();
            }
            else {
              $target.css('height', h);
            }
          });
        })
      })
    }

    $(document).on('click', '[data-popup-open="compare-model"]', function() {
      setHeight();
      setTimeout(function() {
        $("[data-compare-model-popup] .model-row-wrapper").find('.hidden').addClass('visibility-hidden').removeClass('hidden');
      }, 350);
    })

    var $wrapper = $(".model-row-wrapper");
    $(".compare-model-popup:not([data-compare-model-popup]) .model-row-wrapper").find('.hidden').addClass('visibility-hidden').removeClass('hidden');

    $(document).on('click', '.model-row-wrapper .model-row-group.compare .model-subrow', function() {
      if(! $(this).hasClass('active')) {
        $(this).closest('.model-row-group').find('.model-subrow').removeClass('active');
        $(this).addClass('active');
        
        var idx = $(this).attr('data-idx');
        
        $wrapper.find('.model-row-group.highlight .model-subrow').removeClass('active');
        $wrapper.find('.model-row-group.highlight .model-subrow[data-idx="' + idx + '"]').addClass('active');

      }
    })

    $(document).on('click', '.compare-model-popup .see-more', function() {
      $(this).closest('.compare-model-popup').find('.visibility-hidden').toggleClass('visible');
      $(this).toggleClass('active');
    })

  }

}

function productStickyBar() {
  var $option = $("#product-stickybar .product-option");
  $(document).on('click', '#product-stickybar .product-option input', function(){
    var handle = $(this).attr('data-original-handle');
    $("[data-product-form] .product-option").each(function(){
      $(this).find('input[data-original-handle="'+handle+'"]').click();
    })
  })
 
  $(document).on('click', '[data-product-form] .product-option input', function(){
    var handle = $(this).attr('data-original-handle');
    var type = $(this).attr('data-option-type');
    // console.log(handle + "::::::" + type);
    $("#product-stickybar .product-option input[data-option-type='"+ type +"']").prop('checked', false);
    $("#product-stickybar .product-option input[data-option-type='"+ type +"'][data-original-handle='"+ handle +"']").prop('checked', true);

    // $("#product-stickybar .product-option").each(function(){
    //   var $input = $(this).find('input');
    //   if($input.attr('data-option-type') == type) {
    //     if($input.attr('data-original-handle') == handle) {
    //       $input.attr('checked', 'true');
    //     }
    //     else {
    //       $input.removeAttr('checked');
    //     }
    //   }
    // })
  })

  $(document).on('click', '#product-stickybar [data-choose-plan]', function(){
    // $("[data-product-form] [data-choose-plan]").click();
    scrollPage(0, 1000);
  })
}

function atcChoosePlan() {
  var $handle = $("[data-choose-plan], [data-pdp-atc-preorder]");
  $handle.on('click', function(e){
    const $this = $(this);
    e.preventDefault();
    var $form = $(this).closest('form#addToCartForm');

    var _flag = true;
    var $size = $form.find('.product-options.m-size');
    var $s_label = $size.find('.product-options__title');
    if($size.length > 0) {
      var _target = $size.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $s_label.addClass('warning');
        $s_label.find('label').text('Please select a size');
        _flag = false;
      }
    }

    var $color = $form.find('.product-options.m-color');
    var $c_label = $color.find('.product-options__title');

    if($color.length > 0) {
      var _target = $color.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $c_label.addClass('warning');
        $c_label.find('label').text('Please select a color');
        _flag = false;
      }
    }

    if(_flag == false) {
      return false;
    }
    var formType = $form.attr('data-type');
    if($form.length > 0) {
      var isATC = true;
      $.ajax({
        type: 'GET',
        url: '/cart.js',
        dataType: 'json',
        success: function(cart){
          var cartItems = cart['items'];
          for(var i = 0; i < cartItems.length; i ++) {
            var item = cartItems[i];
            var className = '';
            if(formType == 'explore' || formType == 'fit' || formType == 'switch') {
              var itemType = item.properties.device;
              if(itemType == 'explore' || itemType == 'fit' || itemType == 'switch') {
                if(formType != itemType) {
                  var className = formType + '-' + itemType;
                  isATC = false;
                  var $popup = $('[data-popup=cart-err-popup');
                  $popup.find('.text_wrapper').attr('data--hidden', 'true');
                  $popup.find('.text_wrapper.' + className).removeAttr('data--hidden');
                  $popup.fadeIn(300);

                  $('.loader_wrapper').css({"display":"none", "z-index": "-1"});
                  $handle.removeClass('loading');
                  break;
                }
              }
            }
          }

          if(isATC){

            var atcData = [];

            var id = $form.find('select[data-all-variants]').val();
            var data = {
              "id" : id,
              "quantity" : 1,
              "properties": {
                "_id": id,
                "device": formType
              }
            };
            
            window.localStorage.setItem('id', id);
            window.localStorage.setItem('device', formType);

            // document.location.href = '/pages/choose-plan-' + formType;

            var $wrapper = $('.product-main-plan-wrapper');
            var $planWrapper = $wrapper.find('[data-plan-version] .shopify-section.choose-plan');

            if($(window).width() > 750) {
              $planWrapper = $wrapper.find('.hide--mobile [data-plan-version] .shopify-section.choose-plan');
            }
            else {
              $planWrapper = $wrapper.find('.hide--desktop [data-plan-version] .shopify-section.choose-plan');
            }

            var $productWrapper = $wrapper.find('.shopify-section.product-main-section');

            $wrapper.css('min-height', $planWrapper.outerHeight());
            setTimeout(function() {
              $productWrapper.slideUp(800);

              $("#product-stickybar").addClass('disable').removeClass('visible');
              $("header").removeClass('hidden');
              if(document.location.href.includes('/v2')) {
                $("header[data-site-header]").addClass('visibility-hidden');
                $(".custom-product-menu-v2").addClass('visible');

                $('body').css('padding-top', $(".custom-product-menu-v2").height());

              }
  
            }, 10);

            setTimeout(function() {
              $this.removeClass('loading');
              $planWrapper.css('z-index', 1);
              $("[data-single-atc]").show();
              $(".product-plan-faq").removeAttr('data--hidden');

              if(document.location.href.includes('/v2')) {
                // $(".shopify-section.icon-list").attr('data--hidden', 'true');
              }

              scrollPage(0, 300);

            }, 820);
            
          } 
        }
      })
    }
  })

  $(document).on('click', '.choose-plan-back', function() {
    var $wrapper = $('.product-main-plan-wrapper');
    var $planWrapper = $wrapper.find('[data-plan-version] .shopify-section.choose-plan');
    
    if($(window).width() > 750) {
      $planWrapper = $wrapper.find('.hide--mobile [data-plan-version] .shopify-section.choose-plan');
    }
    else {
      $planWrapper = $wrapper.find('.hide--desktop [data-plan-version] .shopify-section.choose-plan');
    }
    
    var $productWrapper = $wrapper.find('.shopify-section.product-main-section');

    $wrapper.css('min-height', $productWrapper.outerHeight());
    $planWrapper.css('z-index', -1);

    setTimeout(function() {
      $("[data-single-atc]").hide();
      $(".product-plan-faq").attr('data--hidden', 'true');
      $productWrapper.slideDown(800);
      $("#product-stickybar").removeClass('disable');
      if(document.location.href.includes('/v2')) {
        // $(".shopify-section.icon-list").removeAttr('data--hidden');
        $("header[data-site-header]").removeClass('visibility-hidden');
        $(".custom-product-menu-v2").removeClass('visible');
        
        $('body').css('padding-top', $("header[data-site-header]").height());
      }
    })

    scrollPage(0, 300);

  })

  var $preOrderHandle = $('[data-pdp-atc-preorder]');
  $preOrderHandle.on('click', function(evt){
    evt.preventDefault();
    var $form = $(this).closest('form#addToCartForm');


    var _flag = true;

    var $size = $form.find('.product-options.m-size');
    var $s_label = $size.find('.product-options__title');
    if($size.length > 0) {
      var _target = $size.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $s_label.addClass('warning');
        $s_label.find('label').text('Please select a size');
        // return false;
        _flag = false;
      }
    }

    var $color = $form.find('.product-options.m-color');
    var $c_label = $color.find('.product-options__title');

    if($color.length > 0) {
      var _target = $color.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $c_label.addClass('warning');
        $c_label.find('label').text('Please select a color');
        // return false;
        _flag = false;
      }
    }

    if(_flag == false) {
      return false;
    }


    var atcData = [];
    var data = {
      id: $form.find('input[data-selected-variant-id]').val(),
      quantity: 1
    }
    atcData.push(data);
    $form.find('.free-order-items .free-order-item.active').each(function(){
      var id = $(this).attr('data-id');
      var data = {
        id: id,
        quantity: 1
      }
      atcData.push(data);
    })

    $.ajax({
      type: 'POST', 
      url: '/cart/add.js',
      dataType: 'json', 
      data: {
        items: atcData
      },
      success: function(){
        document.location.href = '/cart';
      }
    });
  })

  $(document).on('click', '[data-button-no-action]', function(e){
    e.preventDefault();
  })

  // $(document).on('click', '.plan-item .item_wrapper', function() {
  //   $(this).closest('.plan-item').find('button').click();
  // })

}

function collectionTab() {
  var $handle = $(".collection_tab_header .collection_header, .collection_dropdown_menu .collection_header");
  var $content = $(".collection_tab_content .collection_tab_content_item");
  $handle.on('click', function(){
    // if(! $(this).hasClass('active')) {
    //   $handle.removeClass('active');
    //   $(this).addClass('active');
    //   var handle = $(this).attr('data-target');
    //   if(handle == 'all') {
    //     $content.addClass('active');
    //   }
    //   else {
    //     $content.removeClass('active');
    //     $(".collection_tab_content .collection_tab_content_item[data-handle='" + handle + "']").addClass('active');
    //   }

    //   var href_seg = $(this).attr('data-handle');
    //   var url = window.location.href;
    //   url = url.split('/');
    //   url.pop();
    //   url = url.join('/');
    //   url += '/' + href_seg;
    //   window.history.replaceState('', '', url);
    // }
    if($(window).width() < 1200) {
      var $container = $(this).closest('[data-accordions-container]');
      $container.find('[data-accordion-button]').removeClass('is-open');
      $container.find('[data-accordion-content]').slideUp();
      $container.attr('data-open', 'false');
      $container.find('[data-dropdown-bg]').fadeOut(300);
    }
  })
}

function ajaxATC() {
  var $button = $('[data-atc-product-item] [data-ajax-atc-btn]');
  $button.on('click', function(evt){
    var $this = $(this);
    evt.preventDefault();
    var $form = $(this).closest('[data-ajax-form]');
    var planName = $form.attr('data-plan-name');
    var planType = $form.attr('data-plan-type');
    var planId = window.localStorage.getItem('id');

    var __type = $form.attr('data-type');

    var data = {
      "id" : $form.attr('data-id'),
      "quantity" : 1,
    };
    if($form.attr('data-plan-item') == 'true') {
      data['properties'] = {
        '_id': planId,
        'type': 'Subscription',
        'device': planType,
        '_plan': planName
      }
    }
    if($(this).attr('data-plan-validate') == 'true') {
      var isATC = true;
      $.ajax({
        type: 'GET',
        url: '/cart.js',
        dataType: 'json',
        success: function(cart){
          var cartItems = cart['items'];
          
          for(var i = 0; i < cartItems.length; i ++) {
            var item = cartItems[i];
            var className = '';
            if(planType == 'explore' || planType == 'fit' || planType == 'switch') {
              var itemType = item.properties.device;
              if(itemType == 'explore' || itemType == 'fit' || itemType == 'switch') {
                if(planType != itemType) {
                  var className = formType + '-' + itemType;
                  isATC = false;
                  var $popup = $('[data-popup=cart-err-popup');
                  $popup.find('.text_wrapper').attr('data--hidden', 'true');
                  $popup.find('.text_wrapper.' + className).removeAttr('data--hidden');
                  $popup.fadeIn(300);

                  $('.loader_wrapper').css({"display":"none", "z-index": "-1"});
                  $handle.removeClass('loading');
                  break;
                }
              }
            }
          }


          if(isATC) {

            var atcData = [];
            atcData.push(data);

            var _data = {
              "id": planId,
              "quantity": 1,
              "properties": {
                "device": planType,
                "_id": planId,
                "_plan": planName
              }
            }

            atcData.push(_data);


            var img = $(".product-main form#addToCartForm [data-all-variants] option:selected").attr('data-variant-image');

            $.ajax({
              type: 'POST', 
              url: '/cart/add.js',
              dataType: 'json', 
              data: {
                'items': atcData
              },
              success: function(){
                document.location.href = '/cart';
              }
            });

          }

        }
      })
    }
    else {
      if(__type == 'accessory') {
        data['properties'] = {
          "accessory" : "accessory"
        }
      }

      var isPopup = false;

      if($("body").hasClass('template-collection') || $this.closest('.related-products').length > 0) {
        isPopup = true;
      }
      
      if(isPopup) {
        var _img = $this.closest('[data-atc-product-item]').find('.image_warpper').attr('data-img');
        var _title = $this.closest('[data-atc-product-item]').find('.product_title').text();
        updateCartPopup(_img, _title);
      }

      $.ajax({
        type: 'POST', 
        url: '/cart/add.js',
        dataType: 'json', 
        data: data,
        success: function(){
          if(isPopup) {
            // document.location.href = '/cart';
            updateCartNum(1);
            showCartPopup();
          }
          else {
            document.location.href = '/cart';
          }

          // updateCart(true, $this);
        }
      });
    }
  })
}

function initRelatedItem() {
  $("[data-item-variant-selector]").on('change', function(){
    var $form = $(this).closest('[data-ajax-form]');
    $form.attr('data-id', $(this).val());
  })
  $("[data-atc-product-item] [data-variant-color]").on('click', function(){
    var $target = $(this).closest('[data-atc-product-item]').find('[data-ajax-form]');
    $target.attr('data-id', $(this).attr('data-id'));
  })
}

function productTech() {
  $(".show_specs").on('click', function(){
    $(".section__top, .section__bottom").hide();
    $(".section__tech").show();

    // $("#product-stickybar").addClass('active');
    // $("#product-stickybar").addClass('active');

    $("#product-stickybar").removeClass('active').removeClass('visible').addClass('disable');
    $("header.site-header-container").removeClass('hidden');

    $("#product-stickybar .plan_popup").hide();
    $("#product-stickybar .return_tech").show();

    scrollPage(0, 1000);
  })

  $(document).on('click', '.return_tech button', function(){
    $(".section__top, .section__bottom").show();
    $(".section__tech").hide();

    $("#product-stickybar").removeClass('disable');
    $("#product-stickybar .plan_popup").show();
    $("#product-stickybar .return_tech").hide();
    
    if($("#shopify-section-custom-banner-tab").length > 0) {
      var top = $("#shopify-section-custom-banner-tab").offset().top;
      scrollPage(top, 1000);
    }
      
  })


}

function dotsOverlaySlider(){
  var $slider = $("[data-slider-dots-overlay]");
  $slider.on('init', function() {
    $slider.addClass('is-visible');
  })
  $slider.each(function(){
    var length = $(this).attr('data-length');
    $(this).slick({
      arrows: false,
      dots: true
    })
    .on('beforeChange', function(event, slick, currentSlide, nextSlide){
      // console.log('before', nextSlide);
      $(this).find('[data-slider-count]').html(length + ' / ' + (nextSlide + 1));
    })
    .on('afterChange', function(event, slick, currentSlide, nextSlide){
      // console.log('after', nextSlide);
    });
    $(this).find('ul.slick-dots').append('<span data-slider-count>' + length + ' / ' + '1' + '</span>').append('<span data-slick-next-trigger><svg xmlns="http://www.w3.org/2000/svg" width="8.346" height="14.721" viewBox="0 0 8.346 14.721"><path fill="#000" d="M8.19 13.6L1.917 7.357l6.212-6.239a.677.677 0 0 0-.955-.955L0 7.357l7.228 7.2a.677.677 0 0 0 .955-.955z" transform="rotate(180 4.173 7.36)"/></svg></span>');
  })

  $(document).on('click', '[data-slick-next-trigger]', function(){
    var $slider = $(this).closest('[data-slider-dots-overlay]');
    $slider.slick('next');
  })

}

function productPlanPopup() {
  var flag = false;
  var $wrapper = $('.plan_popup');
  var $trigger = $wrapper.find('.popup-trigger');
  var $content = $wrapper.find('.popup-content').find('.popup-header, .content');
  $trigger.on('click', function(){
    var index = $(this).closest('.plan_popup').attr('data-index');
    $wrapper.each(function(){
      if($(this).attr('data-index') != index) {
        $(this).removeClass('active');
      }
    })
    // $(this).closest('.plan_popup').toggleClass('active');
    $(this).closest('.plan_popup').find('.popup-content').fadeIn(300);
  })

  $content.on('mouseover', function(){
    flag = true;
  })
  $trigger.on('mouseover', function(){
    flag = true;
  })
  $content.on('mouseleave', function(){
    flag = false;
  })
  $trigger.on('mouseleave', function(){
    flag = false;
  })
  $("[data-popup-bg]").on('click', function(){

  })
  $(document).on('click', function(){
    if(flag == false) {
      // $wrapper.removeClass('active');
      $wrapper.find('.popup-content').fadeOut(300);
    }
  })

}

function productFeatureTab(){
  var $tabHeader = $(".product-feature-tabs .feature-tab-header");
  var $tabContent = $(".product-feature-tabs .feature-tab-content");
  $tabHeader.on('click', function(){
    if(! $(this).hasClass('active')) {
      $(this).parent().find('.feature-tab-header').removeClass('active');
      $(this).addClass('active');
      var handle = $(this).attr('data-target');
      $tabContent.removeClass('active');
      $(".product-feature-tabs .feature-tab-content[data-handle='"+handle+"']").addClass('active');
    }
  })
}

function variableWidthSlider() {
  var $slider = $("[data-vwidth-slider]");
  $slider.each(function(){
    $(this).slick({
      arrows: false,
      dots: true,
      variableWidth: true
    })
  })
}

function bannerTab() {
  var $wrapper = $(".shopify-section.banner-tab .tab-banner-wrapper");
  var $handler = $wrapper.find('.tab-banner-header').find('.header_item');
  var $content = $wrapper.find('.tab-banner-item');
  $handler.on('click', function(){
    if(! $(this).hasClass('active')) {
      var handle = $(this).attr('data-target');
      // var $target = $(this).closest('.tab-banner-wrapper').find('.tab-banner-item[data-handle="' + handle + '"]');
      $handler.removeClass('active');
      $handler.each(function(){
        $(this).removeClass('active');
      })
      // $content.removeClass('active');
      $(this).addClass('active');
      // $target.addClass('active');
      var idx = $(this).attr('data-index');
      $("[data-banner-slider]").slick('slickGoTo', idx);
    }
  })

  var $slider = $('[data-banner-slider]');
  var options = {
    arrows: true,
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 6000
  }
  if($(window).width() < 768) {
    options['asNavFor'] = '.tab-banner-header';
  }
  $slider.slick(
    options
  )
  .on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $('[data-tab-banner-header][data-index="' + nextSlide + '"]').click();
  })
  .on('afterChange', function(event, slick, currentSlide, nextSlide){
  });


  if($(window).width() < 768) {
    var $headerTarget = $(".shopify-section.banner-tab .tab-banner-header");
    $headerTarget.slick({
      arrows: false,
      dots: false,
      variableWidth: true,
      infinite: true,
      asNavFor: '[data-banner-slider]'
    })
  }

}


function reviewCarousel() {
  var $slider = $("[data-review-slider]");
  $slider.slick({
    arrows: true,
    dots: false,
    variableWidth: true,
    nextArrow: '<button class="slick-next" aria-label="Slider Next"><svg xmlns="http://www.w3.org/2000/svg" width="69" height="69" fill="none" viewBox="0 0 69 69"><circle cx="34.5" cy="34.5" r="34.5" fill="#A1DC00"></circle><path fill="#fff" d="M43.21 35.129c.49-.488.49-1.172.099-1.66L31.785 22.14c-.488-.489-1.27-.489-1.66 0l-.684.683c-.488.488-.488 1.172 0 1.66l9.961 9.766-9.96 9.863c-.489.489-.489 1.172 0 1.66l.683.684c.39.488 1.172.488 1.66 0l11.426-11.328z"></path></svg></button>',
    prevArrow: '<button class="slick-prev" aria-label="Slider Right"><svg xmlns="http://www.w3.org/2000/svg" width="69" height="69" fill="none" viewBox="0 0 69 69"><circle r="34.5" fill="#BAE852" transform="matrix(-1 0 0 1 34.5 34.5)"></circle><path fill="#fff" d="M25.79 35.129c-.49-.488-.49-1.172-.099-1.66L37.215 22.14c.488-.489 1.27-.489 1.66 0l.684.683c.488.488.488 1.172 0 1.66l-9.961 9.766 9.96 9.863c.489.489.489 1.172 0 1.66l-.683.684c-.39.488-1.172.488-1.66 0L25.789 35.129z"></path></svg></button>',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          arrows: false,
          dots: true
        }
      }
    ]
  })
}


function variantChange() {
  var $wrapper = $(".shopify-section.product-grid .product_ajax_form");
  var $handler = $wrapper.find('[data-product-variant-option]').find('input');
  var imgUrl, v_url;
  $handler.on('click', function(){
    imgUrl = $(this).closest('[data-product-variant-option]').attr('data-variant-img');
    v_url = $(this).closest('[data-product-variant-option]').attr('data-variant-url');
    var $image = $(this).closest('.product-grid-item').find('.product_image img');
    var $btn = $(this).closest('.product-grid-item').find('.btn-link');
    $image.removeAttr('srcset').attr('src', imgUrl);
    $btn.attr('href', v_url);
    $(this).closest('[data-link-trigger]').attr('data-target', v_url);
  })

  $(document).on('click', '.product_ajax_form.ajax-atc [data-ajax-atc]', function(e){
    e.preventDefault();
    var $this = $(this);
    var $form = $(this).closest('.product_ajax_form.ajax-atc');
    var option1 = $form.find('input[data-variant-option="1"]:checked').val();
    var option2 = $form.find('select[data-variant-option="2"]').val();
    if(option1 == null || option1 == undefined) {
      option1 = '_BLANK';
    }
    if(option2 == null || option2 == undefined) {
      option2 = '_BLANK';
    }
    var id = $form.find('[data-all-variants]').find('option[data-option1="'+ option1 +'"][data-option2="'+ option2 +'"]').val();

    $.ajax({
      type: 'POST', 
      url: '/cart/add.js',
      dataType: 'json', 
      data: {
        id: id,
        quantity: 1
      },
      success: function(){
        window.location.href = '/cart';
        // updateCart(true, $this);
      }
    });

  })

}

function actionListener(){
  var activeItem = null;
  var activeFlag = false;
  $(document).on('click', 'form#addToCartForm .free-order-items #include_free', function(){
    if($(this).prop('checked') == true) {
      activeItem = $(this).closest('.free-order-items').find('.free-order-item.active');
      if(activeItem.length > 0) {
        activeFlag = true;
      }

      if(activeFlag == true) {
        activeItem.removeClass('active').addClass('deselect');
      }
      else {
        $(this).closest('form').find('.free-order-badge').attr('data--hidden', 'true');
        $(this).closest('form').find('.btn_wrapper button[data-btn-preorder]').attr('data--hidden', 'true');
        $(this).closest('form').find('.btn_wrapper button[data-pdp-atc-preorder]').removeAttr('data--hidden');
      }
    }
    else {
      if(activeFlag == true) {
        $(this).closest('.free-order-items').find('.free-order-item.deselect').removeClass('deselect').addClass('active');
      }
      else {
        $(this).closest('form').find('.btn_wrapper button[data-btn-preorder]').removeAttr('data--hidden');
        $(this).closest('form').find('.btn_wrapper button[data-pdp-atc-preorder]').attr('data--hidden', 'true');
      }
    }
  })

  var is_main_img = true;

  $(document).on('click', 'form#addToCartForm .free-order-items .free-order-item', function(){
    $(this).closest('.free-order-items').find('.free-order-item').removeClass('active');
    $(this).addClass('active');

    $(this).closest('form').find('.btn_wrapper button[data-btn-preorder]').attr('data--hidden', 'true');
    $(this).closest('form').find('.btn_wrapper button[data-pdp-atc-preorder]').removeAttr('data--hidden');

    $(this).closest('form').find('.free-order-badge').attr('data--hidden', 'true');

    var $parent = $(this).closest('.product-main');

    $parent.find('#include_free').prop('checked', false);

    var productHandle = $(this).attr('data-handle');
    var $imgWrapper = $parent.find('.product-images');

    imageUpdates(productHandle, $imgWrapper, true);
    is_main_img = false;
  
  })


  $(document).on('click', 'form[data-type="switch"] input[data-option-type="color"]', function(){
    if(is_main_img == false) {
      imageUpdates('whistle-switch', $(this).closest('.product-main').find('.product-images'), false, true);
      is_main_img = true;
    }
  })

  $(document).on('click', '.sticky-newsletter-close', function(){
    $(this).closest('.sticky-newsletter').addClass('closed');
  })

  

  $(document).on('click', 'form#addToCartForm [data-btn-preorder]', function(e){
    e.preventDefault();
    
    var $form = $(this).closest('form#addToCartForm');

    var _flag = true;
    var $size = $form.find('.product-options.m-size');
    var $s_label = $size.find('.product-options__title');
    if($size.length > 0) {
      var _target = $size.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $s_label.addClass('warning');
        $s_label.find('label').text('Please select a size');
        // return false;
        _flag = false;
      }
    }

    var $color = $form.find('.product-options.m-color');
    var $c_label = $color.find('.product-options__title');

    if($color.length > 0) {
      var _target = $color.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $c_label.addClass('warning');
        $c_label.find('label').text('Please select a color');
        // return false;
        _flag = false;
      }
    }

    if(_flag == false) {
      // return false;
    }

    $(this).closest('form').find('.free-order-badge').removeAttr('data--hidden');
  })

  $(document).on('mousedown', '[data-cart-page-checkout]', function(){
    $(this).addClass('loading');
  })

  $(document).on('click', '[data-loader-btn]', function(){
    $(this).addClass('loading');
  })

  $("[data-scroll-top]").on('click', function(){
    if($(this).closest('.popup').length > 0) {
      $(this).closest('.popup').find('[data-popup-close]').click();
    }
    scrollPage(0, 1000);
  })
  $("[data-scroll-btn]").on('click', function(){
    var top = $($(this).attr('data-target')).offset().top - 30;
    if($(window).width() < 750) {
      if($(this).attr('data-target-mob')) {
        top = $($(this).attr('data-target-mob')).offset().top - 60;
      }
    }
    scrollPage(top, 500);
  })
  $("[data-include-title]").on('click', function(){
    $(this).toggleClass('opened');
    $(this).next('[data-include-content]').slideToggle();
  })

  $(".plan_reason_popup .popup_title").on('click', function(){
    $(this).toggleClass('opened');
    $(this).next('.popup_content').slideToggle();
  })

  $(".popup.upsell-popup").each(function(){
    $(this).find('[data-size-guide]').html($("#shopify-section-custom-size-guide [data-size-guide]").html());
  })

  $("[data-size-guide-trigger]").on('click', function(){
    $(this).closest('.popup').find('[data-size-guide] .size_guide-wrapper').slideToggle();
    if($(window).width() < 768) {
      $(this).closest('.popup-window').animate({
        'scrollTop': $(this).closest('.popup_upsell-item').height() + 50
      }, 500);
    }
  })

  $("[data-cart-item] [data-cart-item-qty]").on('change', function(){
    var qty = $(this).val();
    $(this).closest('[data-cart-item]').attr('data-qty', qty);
    var vId = $(this).closest('[data-cart-item]').attr('data-variant-id');
    var updateQuery = "updates[" +  vId + "]="+ qty;
    var $item = $(this).closest('[data-cart-item]');
    var type = $item.attr('data-type');
    if(type == 'explore' || type == 'fit') {
      // updateQuery += '&updates[' + $item.attr('data-variant-id') + ']=' + qty;
      qty = 0;
      var $target = null;
      if(type == 'explore') {
        $target = $("[data-cart-item].plan-explore:not(.plan-draft)");
        $('[data-cart-item][data-type="explore"]').each(function(){
          qty += parseInt($(this).attr('data-qty'));
        })
      }
      else {
        $target = $("[data-cart-item].plan-fit:not(.plan-draft)");
        $('[data-cart-item][data-type="fit"]').each(function(){
          qty += parseInt($(this).attr('data-qty'));
        })
      }
      if($target != null) {
        updateQuery += '&updates[' + $target.attr('data-variant-id') + ']=' + qty;
      }
    }
    jQuery.post('/cart/update.js', updateQuery, function(){
      window.location.reload();
    });
  })

  if($("[data-product-stickybar]").length > 0) {
    $("#product-stickybar").html($('[data-product-stickybar]').html());
    $('[data-product-stickybar]').remove();
  }

  var $menu = $('[data-menu-link]');
  if($menu.length > 0) {
    var $link = $(".custom-sticky-menu .menu_back a");
    if($menu.attr('data-device') == 'true') {
      $link.attr('href', 'products/whistle-go-explore-gps-pet-tracker-activity-monitor');
      $link.find('span.hide--mobile').text('Back To Whistle Go Explore');
    }
    else {
      // $link.attr('href', $menu.attr('href'));
      $link.attr('href', '/collections/all');
      $link.find('span.hide--mobile').text($menu.text());
    }
  }
  else {
    if(window.page != undefined && window.pageUrl != undefined) {
      if($(".custom-sticky-menu").length > 0) {
        var $target = $(".custom-sticky-menu .menu_back a");
        $target.attr('href', window.pageUrl);
        $target.find('span.hide--mobile').text('Back To ' + window.page);
      }
    }
  }

  $(document).scroll(function(){
    var scroll = $(window).scrollTop();
    var $height = $(window).height();

    var $target = $("[data-product-form] [data-choose-plan], [data-product-form] [data-pdp-atc-preorder]");

    var $sticky = $("#product-stickybar");

    if(! $sticky.hasClass('disable')) {
      if($target.length > 0) {
        if(scroll - $target.offset().top > 100) {
          $("header.site-header-container").addClass('hidden');
          $sticky.addClass('visible');
        }
        else {
          $("header.site-header-container").removeClass('hidden');
          $sticky.removeClass('visible');
        }
      }
    }

    

    // if($('body.template-index').length > 0) {
    //   var $banner = $('.shopify-section.hero-banner');
    //   var height = $banner.offset().top + $banner.height();
    //   if(scroll > height && scroll < height + 500) {
    //     $(".site-header-container").addClass('show-newsletter');
    //   }
    //   else {
    //     $(".site-header-container").removeClass('show-newsletter');
    //   }
    // }

  })

}

function scrollPage(top, time) {
  $("html, body").animate({
    'scrollTop': top
  }, time)
}


export default function(){
  init();
}