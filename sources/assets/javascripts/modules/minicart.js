import $ from 'jquery';
import CartJS from '/vendor/cart.min.js';
import tinybind from 'tinybind';
import { formatMoney } from '/tools/moneyFormats';

var $atcButton = null;

const $variantsSelector = $('[data-all-variants]');
let data = {
  cart: {},
  update(newCart) {
    this.cart = newCart
  },
  setRedirect(e) {
    e.preventDefault();
    goToCheckout();
  }
};
let open = false;
const $minicart = $('[data-minicart]');

let _form = null;


const updateCartNum = function(num) {
  var $cartCount = $("[data-cart-count]");
  $cartCount.text(parseInt(($cartCount.eq(0)).text()) + num);
}

const updateCartPopup = function(img, title) {
  var $cartPopup = $("[data-cart-popup]");
  $cartPopup.find('.item_image').html('<img src="' + img + '">');
  $cartPopup.find('.item_title').text(title);
}

const showCartPopup = function() {
  var $cartPopup = $("[data-cart-popup]");
  $cartPopup.show();
  setTimeout(function() {
    $cartPopup.addClass('visible');
  }, 10);
  setTimeout(function() {
    $cartPopup.fadeOut(300);
    setTimeout(function() {
      $cartPopup.removeClass('visible');
    }, 310);
  }, 3000);
}


const addToCart = function(id, qty, properties, $variant) {
  let data = {
    "quantity": qty,
    "id": id,
    "properties": {}
  }
  if (properties['shipping_interval_frequency'] && properties['shipping_interval_unit_type'] && properties['subscription_id']) {
    data["properties"]["shipping_interval_frequency"] = properties['shipping_interval_frequency'];
    data["properties"]["shipping_interval_unit_type"] = properties['shipping_interval_unit_type'];
    data["properties"]["subscription_id"] = properties['subscription_id'];
  }

  updateCartPopup($variant.attr('data-variant-image'), productTitle);

  $.ajax({
    type: 'POST',
    url: '/cart/add.js',
    data: data,
    dataType: 'json',
    success: function () {
      // open = true;
      
      if(_form != null) {
        // updateMinicart(_form.find('[data-loader-btn]'));
      }
      else {
        // updateMinicart();
      }

      updateCartNum(1);
      showCartPopup();

      if($atcButton != null) {
        $atcButton.removeClass('loading');
      }

      // document.location.href = '/cart';
    },
    error: function () {
      console.error("Can't add item to cart")
    }
  });
}

const getProperties = function($productForm ) {
  let properties = []
  $productForm.find('[data-product-property]').each(function (index, element) {
    let key = $(element).data('product-property');
    let value = $(element).val();
    if (key && key != 'hidden' && value) {
      properties[key] = value;
    }
  });
  return properties;
}

const addToCartHandler = function ($productForm) {
  const $variantSelected = $('option:selected', $productForm.find('[data-all-variants]'));
  let variantId = $variantSelected.val();
  if ($variantSelected.data('discount-variant-id') != '') {
    variantId = $variantSelected.data('discount-variant-id');
  }
  const qty = $productForm .find('[data-qty]').val() || 1;
  let properties = getProperties($productForm);
  if (variantId) {
    addToCart(variantId, qty, properties, $variantSelected);
    _form = $productForm;
  } else if ($productForm.find('[data-all-variants] option:selected').data('multi-variants')) {
    let $variants = $productForm.find('[data-all-variants] option:selected').data('multi-variants');
    $variants.forEach(function(id) {
      addToCart(id, qty, properties, $variantSelected);
      $productForm.find('[data-loader-btn]').removeClass('loading');
    });
  } else {
    console.error("minicart.js: can't add item to cart, wrong variantId", variantId);
  }
}


const removeFromCart = function () {
  if ($(this).data('line') != undefined) {
    let id = $(this).data('line');
    if (id != undefined) {
      var $items = $(this).closest('.minicart__items');
      var $item = $(this).closest('.minicart__item');
      var handle = $item.attr('data-handle');
      if(handle != '' && handle != undefined && handle != null) {
        var updateData = [];
        $items.find('.minicart__item').each(function() {
          var $this = $(this);
          if($this.attr('data-handle') == handle) {
            updateData.push(0);
          }
          else {
            updateData.push($this.attr('data-qty'));
          }
        })
        jQuery.post('/cart/update.js', {updates: updateData}, function(){
          // updateMinicart();
          document.location.href = '/cart';
        });
      }
      else {
        CartJS.removeItem(id + 1);
      }
    } else {
      console.error("Can't remove item", id, " from the cart!");
    }
  } else {
    console.error("Can't remove item to the cart since data-line is not present in the remove-from-cart button!");
  }
}

function reChargeProcessCart() {
  let token = '';
	function get_cookie(name){ return( document.cookie.match('(^|; )'+name+'=([^;]*)')||0 )[2] }
	do {
      		token=get_cookie('cart');
	}
	while(token == undefined);

	try { var ga_linker = ga.getAll()[0].get('linkerParam') } catch(err) { var ga_linker ='' }
	var customer_param = '{% if customer %}customer_id={{customer.id}}&customer_email={{customer.email}}{% endif %}'
	document.location.href = "https://checkout.rechargeapps.com/r/checkout?myshopify_domain="+myshopify_domain+"&cart_token="+token+"&"+ga_linker+"&"+customer_param;
}

const goToCheckout = function() {
  let hasSubscription = false;
  for(let i = 0; i < data.cart.items.length; i++) {
    let item = data.cart.items[i];
    if(item.product_title.includes('Auto renew')) {
      hasSubscription = true;
    }
  }

  if(hasSubscription) {
    reChargeProcessCart();
  } else {
    document.location.href = "/checkout";
  }
}

export function updateCart(flag, $trigger) {
  if(flag !== false) {
    open = true;
    updateMinicart($trigger);
  }
}

const updateMinicart = function($trigger) {

  $.ajax({
    url: "/cart.js",
    type: "GET",
    dataType: "json",
    success: function(result) {
      if($trigger) {
        $trigger.removeClass('loading');
      }
      data.update(result);    
      updateCartCount();
      if (open) {
        $minicart.addClass('is-open');
        $("html, body").addClass('no-scroll')
        $("body").addClass('no-scroll minicart-open');
        open = false;
      }
    }
  })

}

const updateCartCount = function() {
  let cartCount = '[data-cart-count]';
  $(cartCount).text(data.cart.item_count);
}

const eventHandlers = function() {

  var url = document.location.href;
  if(url.includes('/cart=open')) {
    open = true;
    updateMinicart();
  }

  $(document).on('click','[data-add-to-cart]', function(e) {
    e.preventDefault();
    $('[data-popup]').addClass('is-hidden');

    $atcButton = $(this);

    var $form = $(this).closest('form#addToCartForm');
    var _flag = true;
    var $size = $form.find('.product-options.m-size');
    var $s_label = $size.find('.product-options__title');
    if($size.length > 0) {
      var _target = $size.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $s_label.addClass('warning');
        $s_label.find('label').text('Please select a size');
        _flag = false;
      }
    }

    var $color = $form.find('.product-options.m-color');
    var $c_label = $color.find('.product-options__title');

    if($color.length > 0) {
      var _target = $color.find('input[data-option-type].selected');
      if(_target.length > 0) {
      }
      else {
        $c_label.addClass('warning');
        $c_label.find('label').text('Please select a color');
        _flag = false;
      }
    }

    if(_flag == false) {
      return false;
    }

    addToCartHandler($(this).closest('[data-product-form]'));
  });

  $(document).on('click', '[data-remove-from-cart]', removeFromCart);

  $('[data-minicart-toggle]').on('click', function(e) {
    e.preventDefault();
    $minicart.toggleClass('is-open');
    $("html, body").toggleClass('no-scroll');
    $("body").toggleClass('minicart-open');
  });


  $(document).on('click', '.custom-qty-dropdown .selected-option', function() {
    $(this).closest('.custom-qty-dropdown').toggleClass('open');
  })

  $(document).on('blur', '.custom-qty-dropdown .selected-option', function() {
    $(this).closest('.custom-qty-dropdown').removeClass('open');
  })

  $(document).on('click', '.minicart-qty .options-list li', function() {
    var val = $(this).attr('value');
    $(this).closest('.minicart-qty').find('.selected-option').attr('data-value', val);
    var handle = $(this).closest('.minicart__item').attr('data-handle');
    var $items = $(this).closest('.minicart__items');
    var updateData = [];
    $items.find('.minicart__item').each(function() {
      var $this = $(this);
      var itemVal = $this.find('.minicart-qty .selected-option').attr('data-value');
      if(handle != '' && handle != undefined && handle != null) {
        if($this.attr('data-handle') == handle) {
          updateData.push(val);
        }
        else {
          updateData.push(itemVal);
        }
      }
      else {
        updateData.push(itemVal);
      }
    })
    jQuery.post('/cart/update.js', {updates: updateData}, function(){
      // updateMinicart();
      document.location.href = '/cart';
    });
  })

  var flag = false;
  $(document).on('mouseover', '.custom-qty-dropdown', function() {
    flag = true;
  })
  $(document).on('mouseleave', '.custom-qty-dropdown', function() {
    flag = false;
  })

  $(document).on('click', '*', function() {
    if(!flag) {
      $('.custom-qty-dropdown').removeClass('open');
    }
  })



  $(document).on('cart.requestComplete', function (event, cart) {
    updateMinicart();

  });
  $(document).ready(function() {
    if(window.localStorage.getItem('cart_state') == 'open') {
      open = true;
      localStorage.setItem('cart_state', 'close');
    }
    updateMinicart();
  });
}

const init = function() {
  if (tinybind) {
    tinybind.formatters.formatMoney = formatMoney;
    tinybind.formatters.isDefaultTitle = function (title) {
      if (title.toLowerCase() == "default title") {
        return false;
      }
      return true;
    };
    tinybind.formatters.append = function(value, append) {
      return value+append;
    }
    tinybind.formatters.convert = function(item, append) {
      var plan = item['properties']['_plan'];
      var type = item['properties']['type'];
      if(type == undefined && plan == undefined) {
        return item['url'];
      }
      else if(plan != '' && plan != undefined && plan != null && type != 'Subscription') {
        if (append) {
          return '/products/'+append;
        }
        return item['url'];
      }
      else {
        return 'javascript:;';
      }
    }
  
    tinybind.formatters.isItem = function(count) {
      if(count > 0) {
        return true;
      }
      return false;
    }
    
    tinybind.formatters.isEmpty = function(count) {
      if(! count > 0) {
        return true;
      }
      return false;
    }

    tinybind.formatters.isSubscription = function(item) {
      var plan = item['properties']['_plan'];
      var type = item['properties']['type'];
      if(plan != '' && plan != undefined && plan != null) {
        if(type != 'Subscription') {
          return true;
        }
        return false;
      }
      return false;
    }

    tinybind.formatters.isSubscription = function(item) {
      var plan = item['properties']['_plan'];
      var type = item['properties']['type'];
      if(plan != '' && plan != undefined && plan != null) {
        if(type != 'Subscription') {
          return true;
        }
        return false;
      }
      return false;
    }

    tinybind.formatters.isSubscriptionDiscount = function(item) {
      var plan = item['properties']['_plan'];
      var type = item['properties']['type'];
      if(plan != '' && plan != undefined && plan != null) {
        if(type != 'Subscription' && plan != 'monthly') {
          return true;
        }
        return false;
      }
      return false;
    }


    
    tinybind.formatters.isSubscriptionFreeItem = function(item) {
      var plan = item['properties']['_plan'];
      var type = item['properties']['type'];
      if(plan != '' && plan != undefined && plan != null) {
        if(type == 'Subscription') {
          return true;
        }
        return false;
      }
      return false;
    }

    tinybind.formatters.isSubscriptionPlanItem = function(item) {
      var plan = item['properties']['_plan'];
      var type = item['properties']['type'];
      if(plan != '' && plan != undefined && plan != null) {
        if(type == 'Subscription' && plan != 'monthly') {
          return true;
        }
        return false;
      }
      return false;
    }

    
    tinybind.formatters.isSubscriptionPlanMonthlyItem = function(item) {
      var plan = item['properties']['_plan'];
      var type = item['properties']['type'];
      if(plan != '' && plan != undefined && plan != null) {
        if(type == 'Subscription' && plan == 'monthly') {
          return true;
        }
        return false;
      }
      return false;
    }

    tinybind.formatters.isItemFit = function(item) {
      var device = item['properties']['device'];
      if(device == 'fit') {
        return true;
      }
      return false;
    }

    tinybind.formatters.isItemExplore = function(item) {
      var device = item['properties']['device'];
      if(device == 'explore' || device == 'switch') {
        return true;
      }
      return false;
    }

    

    tinybind.formatters.unlessItemFit = function(item) {
      var device = item['properties']['device'];
      if(device != 'fit') {
        return true;
      }
      return false;
    }

    tinybind.formatters.isUpsell = function(cart) {
      var items = cart['items'];
      for(var i = 0; i < items.length; i ++) {
        var item = items[i];
        var type = item['properties']['_type'];
        if(type == 'upsell') {
          return true
        }
      }
      return false;
    }

    tinybind.formatters.isPlan = function(cart) {
      var items = cart['items'];
      for(var i = 0; i < items.length; i ++) {
        var plan = items[i]['properties']['_plan'];
        if(plan != undefined && plan != null && plan != '') {
          return true;
        }
      }
      return false;
    }

    tinybind.formatters.planName = function(cart) {
      var items = cart['items'];
      var plan = ''
      for(var i = 0; i < items.length; i ++) {
        var _plan = items[i]['properties']['_plan'];
        if(_plan == '2-year-plan') {
          plan = 'biannual';
          return;
        }
        else if(_plan == '1-year-plan' && plan != 'biannual') {
          plan = 'annual';
        }
        else if(_plan == 'monthly' && plan == '') {
          plan = _plan;
        }
      }
      return plan;
    }

    tinybind.formatters.isDefault = function(item) {
      var plan = item['properties']['_plan'];
      if(plan == '' || plan == undefined || plan == null) {
        return true;
      }
      return false;
    }

    tinybind.formatters.discountPrice = function(item, price) {
      var plan = item['properties']['_plan'];
      var device = item['properties']['device'];
      var discount = 0;
      if(plan != '' && plan != undefined && plan != null) {
        if(item['properties']['type'] != 'Subscription') {
          if((device == 'explore' || device == 'switch') && plan != 'monthly') {
            discount = 2000;
          }
          if(device == 'fit' && plan != 'monthly') {
            discount = 1500;
          }
        }
      }
      return price - discount;
    }

    tinybind.formatters.savePrice = function(item, price) {
      var plan = item['properties']['_plan'];
      var device = item['properties']['device'];
      var discount = 0;
      if(plan != '' && plan != undefined && plan != null) {
        if(item['properties']['type'] != 'Subscription') {
          if(device == 'explore' || device == 'switch') {
            discount = 2000;
          }
          if(device == 'fit') {
            discount = 1500;
          }
        }
      }
      return Math.round(discount / price * 100);
    }

    tinybind.formatters.itemHandle = function(item) {
      var handle = '';
      var plan = item['properties']['_plan'];
      if(plan != '' && plan != undefined && plan != null) {
        handle = item['properties']['_plan'] + '-' + item['properties']['_id'];
      }
      return handle;
    }

    tinybind.formatters.featureText = function(item, id) {
      if(item.product_id == id) {
        return true;
      }
      // var device = item['properties']['device'];
      // if(device == _device) {
      //   return true;
      // }
      return false;
    }

    tinybind.formatters.isDevice = function(cart) {
      var items = cart['items'];
      for(var i = 0; i < items.length; i ++) {
        var item = items[i];
        var id = item['product_id'];
        if(id == 6574946975792) {
          return false
        }
      }
      return true;
    }

    tinybind.formatters.isSwitch = function(cart) {
      var items = cart['items'];
      for(var i = 0; i < items.length; i ++) {
        var item = items[i];
        var id = item['product_id'];
        if(id == 6574946975792) {
          return true
        }
      }
      return false;
    }

    tinybind.formatters.qtyActive = function(qty, _qty) {
      if(qty == _qty) {
        return 'active';
      }
      return '';
    }

    tinybind.bind($minicart, data);
  } else {
    console.error("minicart.js: Tinybind template library is not connected");
    return false;
  }

  if(localStorage.getItem('opened_side_cart') == 'true' && window.location.pathname == '/' ) {
    $('[data-minicart]').addClass('is-open');
    $("html, body").addClass('no-scroll');
    localStorage.setItem('opened_side_cart', false);
  }
  
}

export default function() {
  init();
  eventHandlers();
  updateCart(false);
}
