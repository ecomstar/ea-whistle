import $ from 'jquery';

export default function() {
  $('[data-popup-close]').click(function(evt){
    evt.preventDefault();
    // $(this).closest('[data-popup]').addClass('is-hidden');
    $(this).closest('[data-popup]').fadeOut(300);
  })

  $(document).on('click', '[data-popup-open]', function(event) {
    event.preventDefault();
    var popup = $(this).data('popup-open');
    var $popup = $('[data-popup="'+ popup +'"');
    // $popup.toggleClass('is-hidden');
    $popup.fadeIn(300);
  });

  // Open popup when popup form loaded with errors
  $('[data-popup-form-errors]').each(function (index, element) {
    const popup = $(this).data('popup-form-errors');
    $(`[data-popup-open="${popup}"]`).trigger('click');
  });
}