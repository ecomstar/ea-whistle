import $ from 'jquery';

export default function(){

  var windowHeight = $(window).height();
  $(document).scroll(function(){
    var scrollTop = $(document).scrollTop();

    var $video = $('[data-video-wrapper][data-scroll-play]');

    $video.each(function(){
      var $this = $(this);
      var $target;
      var handle = $this.attr('data-type');

      var pos_t = $this.offset().top;

      if(handle == 'video') {
        $target = $this.find('video');
      }
      else if(handle == 'external_video') {
        $target = $this.find('iframe');
      }
     
      if(scrollTop + windowHeight > pos_t ) {
        if(scrollTop < pos_t + $target.height()) {
          if($this.attr('data-screen') != 'off' && $this.attr('data-play') != 'true') {
            if(handle == 'video') {
              $target.get(0).play();
            }
            else if(handle == 'external_video') {
              var src = $target.attr('src');
              if(src.indexOf('vimeo') > -1) {
                src += '?autoplay=1&muted=1&loop=1';
              }
              else {
                src += '?rel=0&loop=1&title=0&portrait=0&autoplay=1&playsinline=1&muted=1';
              }
              setTimeout(function(){
                $target.attr('src', src);
              }, 150);
            }
            $this.attr('data-play', 'true');
          }
        }
        else {
          $this.attr('data-screen', 'off');
        }
      }


    })

  })



}