import $ from 'jquery';

const animation = function () {
  const sections = $('.section, [data-animate-section]');
  const blocks = $('[data-animate-block], [data-custom-block]');
  initAnimation();
  $(document).scroll(function () {
    initAnimation();
  }).scroll();
  function initAnimation(){
    const scroll = $(document).scrollTop();
    const windowHeight = $(window).outerHeight();
    const headerHeight = $('[data-site-header]').outerHeight();
    sections.each(function (index, section) {
      // const sectionTop = $(section).position().top - $(window).outerHeight()/1.5;
      const sectionTop = $(section).offset().top - $(window).outerHeight();
      if (scroll >= sectionTop + 150) {
        $(section).addClass('section-in');
      }
    });
    blocks.each(function (index, block) {
      const blockTop = $(block).offset().top;
      const blockHeight = $(block).outerHeight();
      var scrollMargin = 0;
      if($(window).width() < 768) {
        // scrollMargin = 150;
      }
      if ( $(block).find('[data-scroll-line]').length ) {
        let lineHeight = 0;
        let $lastActiveBlock = $(block).find('[data-animate-block].scroll-line-active').last();
        if ($lastActiveBlock.length) {
          const lastActiveBlockHeight = $lastActiveBlock.outerHeight();
          // lineHeight = 100*(scroll + headerHeight + windowHeight / 2 - blockTop - lastActiveBlockHeight + 150) / blockHeight;
          lineHeight = 100 * (scroll + headerHeight + windowHeight / 2  - blockTop + scrollMargin) / blockHeight;
          // console.log(scroll + headerHeight + windowHeight / 2  - blockTop);
        } else {
          $lastActiveBlock = $(block).find('[data-animate-block]').eq(0)
        }
        var imgUrl = $lastActiveBlock.data('image');
        if($lastActiveBlock.attr('data-available') == 'true') {
          $('[data-scroll-image]').attr('src', imgUrl);
        }
        
        if (lineHeight >= 100) {
          $(block).addClass('scroll-line-active');
          $('[data-scroll-line]', $(block)).css('height', '100%');
        } else {
          $(block).removeClass('scroll-line-active');
          $('[data-scroll-line]', $(block)).css('height', lineHeight+'%');
        }

      } else {

        var m = 0;
        if($(block).attr('data-delay') == 'true') {
          m = 150;
        }
        if(scroll + windowHeight > $(block).offset().top) {
          $(block).addClass('pre-active');
        }
        else {
          $(block).removeClass('pre-active');
        }
        if(scroll + windowHeight > $(block).offset().top + 150 + m) {
          $(block).addClass('is-active');
        }
        else {
          $(block).removeClass('is-active');
        }

        if (scroll + headerHeight + windowHeight / 2 >= blockTop - scrollMargin) {
          $(block).addClass('scroll-line-active');

          $(block).find('p[data-animate-block]').each(function(){
            if(scroll + headerHeight + windowHeight / 2 >= $(this).offset().top - scrollMargin) {
              $(this).addClass('active');
            }
            else {
              $(this).removeClass('active');
            }
          })

        } else {
          $(block).removeClass('scroll-line-active');
        }
      }
    });
  }
  $(document).ready(function () {
    $('body').addClass('body-in');
  });
}

export default animation;