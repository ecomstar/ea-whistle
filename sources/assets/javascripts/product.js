import $ from 'jquery';


import product from './modules/product';

window.$ = window.jQuery = $;

const register = function (f, args) {
  try {
    f(args);
    if (args && args.resize) {
      $(window).resize(function() {
        f(args);
      })
    }
  } catch (e) {
    console.error(e);
  }
}

$(document).ready(() => {
  register(product);
});
